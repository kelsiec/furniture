package damage;

public enum DamageType {
    BLUNT, FIRE
}