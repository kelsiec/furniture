package furniture;

import materials.Material;

public class Table extends Furniture {
    public Table(Material material) {
        super("table", material);
    }
}
