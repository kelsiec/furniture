package furniture;

import materials.Material;

public class Chair extends Furniture {
    public Chair(Material material) {
        super("chair", material);
    }
}
